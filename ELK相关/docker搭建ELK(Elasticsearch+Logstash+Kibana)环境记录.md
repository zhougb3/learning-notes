# Docker搭建ELK(Elasticsearch+Logstash+Kibana)分布式日志平台



### 前言

##### ELK目的

ELK就是一个集中式的日志系统，将分布在各个地区的日志汇总集合到一个地方来，各个业务进程要将日志写到Redis集群。

##### ELK工作流程

应用将日志按照约定的Key写入Redis，Logstash从Redis中读取日志信息写入ElasticSearch集群。Kibana读取ElasticSearch中的日志，并在Web页面中以表格/图表的形式展示。 

##### 环境

目的是在window docker上搭建三个docker，各自功能如下：

| 服务器名 | IP         | 说明                          |
| -------- | ---------- | ----------------------------- |
| es1      | 172.17.0.2 | 部署ElasticSearch主节点       |
| es2      | 172.17.0.3 | 部署ElasticSearch从节点       |
| elk      | 172.17.0.4 | 部署Logstash + Kibana + Redis |

使用的软件版本如下：

| 项            | 说明              |
| ------------- | ----------------- |
| Linux Server  | Ubuntu：16.04镜像 |
| Elasticsearch | 6.0.0             |
| Logstash      | 6.0.0             |
| Kibana        | 6.0.0             |
| Redis         | 3.0.6             |
| JDK           | 1.8               |

### 搭建流程

##### 构建JDK环境

1. 官网下载jdk的压缩包（我的是jdk1.8.0_201）,创建文件夹存放解压后的jdk1.8.0_201文件夹

2. 由于三个docker都需要用Java环境，因此我使用以下的dockerfile文件构建镜像（在1中的文件夹同级目录下创建dockerfile文件）

   ```
   # 以ubuntu为基础镜像
   FROM ubuntu:16.04
   # 指定维护者信息
   MAINTAINER mateng 789456123@qq.com
   # 增加宿主机的文件夹到docker ubuntu的目录
   ADD jdk1.8.0_201 /usr/local/jdk1.8.0_201
   # 设置ubuntu的环境变量
   ENV JAVA_HOME /usr/local/jdk1.8.0_201
   ENV CLASSPATH $JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
   ENV PATH $PATH:$JAVA_HOME/bin
   # 设置字符集，防止乱码
   ENV LANG en_US.utf8
   ENV LC_ALL en_US.utf8
   # 设置时区
   RUN rm -rf /etc/localtime && ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
   # 安装telnet/ifconfig（用于调测网络使用）,-y用于自动回复
   RUN apt-get update
   RUN apt-get -y install telnet net-tools wget vim curl
   ```

3. 运行命令`docker build -t java8ubuntu .`

##### 构建ELK环境

1. 对于三个docker容器都需要用到的数据，我也采取构建docker镜像的方式（貌似这么用不太规范），dockerfile如下：

   ```
   # 以ubuntu为基础镜像
   FROM java8ubuntu
   # 指定维护者信息
   MAINTAINER mateng 789654123@qq.com
   # 运行安装等命令
   RUN apt-get update
   RUN apt-get -y install tar
   RUN useradd elk
   RUN mkdir /usr/elk
   RUN mkdir /home/elk
   RUN chown -R elk:elk /usr/elk
   RUN chown -R elk:elk /home/elk
   RUN wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.0.0.tar.gz
   RUN wget https://artifacts.elastic.co/downloads/kibana/kibana-6.0.0-linux-x86_64.tar.gz
   RUN wget https://artifacts.elastic.co/downloads/logstash/logstash-6.0.0.tar.gz
   RUN tar -zvxf elasticsearch-6.0.0.tar.gz
   RUN tar -zvxf logstash-6.0.0.tar.gz
   RUN tar -zvxf kibana-6.0.0-linux-x86_64.tar.gz
   ```

2. 执行命令`docker build -t elkubuntu .`

##### 构建ElasticSearch容器

1. 创建es1容器，`docker run -it --name elkubuntu_bridge_mainsearch -p 9200:9200 -p 9300:9300 elkubuntu /bin/bash`
2. 创建es2容器：`docker run -it --name elkubuntu_bridge_secondsearch -p 9201:9201 -p 9301:9301 elkubuntu /bin/bash` (这里也可以不作端口映射，外界访问时只需要访问es1即可)
3. 在两个docker上运行以下命令：

```
mv /elasticsearch-6.0.0 /usr/elk
chown -R elk:elk /usr/elk/elasticsearch-6.0.0/
su - elk
mkdir /home/elk/es
mkdir /home/elk/es/data
mkdir /home/elk/es/logs
```

4. 执行`vim /usr/elk/elasticsearch-6.0.0/config/elasticsearch.yml`，如果找不到vim则切换为root再执行，编辑完成后记得切换回来。在es1上配置如下：

```
cluster.name: es 
node.name: es1
path.data: /home/elk/es/data
path.logs: /homeelk/es/logs
network.host: 172.17.0.2
http.port: 9200
transport.tcp.port: 9300
node.master: true
node.data: true
discovery.zen.ping.unicast.hosts: ["172.17.0.2:9300","172.17.0.3:9300"]
discovery.zen.minimum_master_nodes: 1
```

​	在es2上配置如下：

```
cluster.name: es 
node.name: es2
path.data: /home/elk/es/data
path.logs: /homeelk/es/logs
network.host: 172.17.0.3
http.port: 9200
transport.tcp.port: 9300
node.master: false
node.data: true
discovery.zen.ping.unicast.hosts: ["172.17.0.2:9300","172.17.0.3:9300"]
discovery.zen.minimum_master_nodes: 1
```

5. 在两台docker上分别`vim /home/elk/.bash_profile`，加入以下内容，之后执行`source .bash_profile`命令

```
export JAVA_HOME=/home/openam_jxdoe/jdk1.8.0_201
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
```

6. 在两台docker上各自运行`/usr/elk/elasticsearch-6.0.0/bin/elasticsearch`，如果有报错则解决报错。
7. 查看健康状态：`curl http://10.0.75.2:9200/_cluster/health`， 如果看到返回包含`status=green`的json字符串则成功了。

##### 构建ElasticSearch容器过程探讨

1. 在window上运行docker时，是会有虚拟工具（hyper-v）虚拟出Linux内核和网卡的，一般这个虚拟地址为`10.0.75.1`（对外DockerNAT。可在window下执行ipconfig看到），而对内则为`10.0.75.2`（可在docker容器内ifconfig看到），因此我们从外部访问docker容器内部服务时用的是这个地址。
2. 这里构建的两个docker是采用docker0网桥的模式，其实我们也可以使用`--net host`来直接映射所有端口。使用docker0网桥时docker0的IP地址一般为`172.17.0.1`，我们创建的docker则依次分配IP地址。
3. 在编辑yml文件时要注意格式，尽量保持冒号前面没空格，后面一个空格，不要用tab键，否则可能报`'log4j2.`错误。 
4. **在上述执行第五步骤时如果出现主节点可以运行，一旦从节点运行则崩溃（被kill掉），则很大可能是docker内存问题。我上window docker的设置里面将内存和CPU限制调大则无此问题了**。

##### 部署Logstash + Kibana + Redis 

```
//之前我在执行的时候忘记加上5601端口的映射了，这里要加上，如下
docker run -it --name elkubuntu_bridge_redis -p 6379:6379 -p 5601:5601 elkubuntu /bin/bash`
apt-get install redis-server
./usr/bin/redis-server &    //让Redis在后台运行
mv /logstash-6.0.0 /usr/elk
chown -R elk:elk /usr/elk/logstash-6.0.0/
su - elk
mkdir /home/elk/logstash
mkdir /home/elk/logstash/data
mkdir /home/elk/logstash/logs
```

运行`vim /usr/elk/logstash-6.0.0/config/logstash.yml`增加以下内容

```
path.data: /elk/logstash/data
path.logs: /elk/logstash/logs
```

新增文件`/usr/elk/logstash-6.0.0/config/input-output.conf`，内容如下：

```
input {
  redis {
    data_type => "list"
    key => "logstash"
    host => "172.17.0.4"
    port => 6379
    threads => 5
    codec => "json"
  }
}
filter {
}
output {
  elasticsearch {
    hosts => ["172.17.0.2:9200","172.17.0.3:9200"]
    index => "logstash-%{type}-%{+YYYY.MM.dd}"
    document_type => "%{type}"
  }
  stdout {
  }
}
```

运行命令：`/usr/elk/logstash-6.0.0/bin/logstash -f config/input-output.conf`，在启动输出的最后一行看到如下信息则启动Logstash成功：

```
[INFO ][logstash.pipeline        ] Pipeline started {"pipeline.id"=>"main"}
[INFO ][logstash.agent           ] Pipelines running {:count=>1, :pipelines=>["main"]}
```

执行以下命令以部署kibana

```
mv /kibana-6.0.0-linux-x86_64 /usr/elk/
mv ./kibana-6.0.0-linux-x86_64/ ./kibana-6.0.0 //重命名
chown -R elk:elk /usr/elk/kibana-6.0.0/
```

kibana需要开放5601端口，由于我们在一开始的时候忘记进行端口映射，这里需要补充进行端口映射。(如果你执行命令时有加上则跳过此步骤)

```
docker commit 6c6782c79e84 elkubuntu_bridge_latest_redis
docker run -it --name elkubuntu_bridge_latest_es3 -p 5601:5601 elkubuntu_bridge_latest_redis /bin/bash
```

修改配置：

```
vim /usr/elk/kibana-6.0.0/config/kibana.yml
//增加配置如下：
server.port: 5601
server.host: "172.17.0.4"
elasticsearch.url: "http://172.17.0.2:9200"
```

接着执行命令`/usr/elk/kibana-6.0.0/bin/kibana`即可启动服务。



##### 测试

1. 启动Redis客户端写入日志如下：

   ```
   lpush logstash '{"host":"127.0.0.1","type":"logtest","message":"hello"}'
   ```

2. 浏览器访问`10.0.75.2:5601`点击create再进入`10.0.75.2:5601/app/kibana#/discover  `即可看到日志。

### 参考

[[ELK6.0部署：Elasticsearch+Logstash+Kibana搭建分布式日志平台]](https://ken.io/note/elk-deploy-guide)







