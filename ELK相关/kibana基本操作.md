# ELK使用——kibana基本操作

在ELK环境搭建之后，我们在后续的使用上基本都是在kibana做一些日志查询和统计。打开kibana的主页面，如下图：

![kibana](https://gitlab.com/zhougb3/learning-notes/raw/master/ELK%E7%9B%B8%E5%85%B3/kibana.png)

图中标号1的方框中三个功能`discover`，`visualize`， `Dashboard`是我们最常用的的三个功能，以下分为三个部分讲解。

##### Discover

图中标号2的方框是用来输入我们的查询条件。可以输入[Elasticsearch 查询语句](https://link.juejin.im/?target=https%3A%2F%2Fwww.elastic.co%2Fguide%2Fen%2Felasticsearch%2Freference%2F5.4%2Fquery-dsl-query-string-query.html%23query-string-syntax)查询你的数据 。比较常见的搜索方式是通过UID和进程名搜索日志如搜索：`1743711764 AND live_game_room_d`。可以在指定字段查找，可以区间查找也可以带上逻辑表达式查找：`line_id:[1 TO 100] AND message:1743711764`。

图中标号3的地方有限定查找的时间范围。同时，可以点击`save`将查找的结果保存，之后可通过`open`打开。

在图中正中间部分，上面则显示了查询的日志量与时间的分布，下面则显示了查询结果。查询的内容默认都显示在`_source`中。

图中标号4的方框中显示了查询结果的所有字段名（即显示的_source内容中的字段名），将鼠标移动到字段名上面，我们可以点击`add`来展示指定的字段即好。

![kibana2](https://gitlab.com/zhougb3/learning-notes/raw/master/ELK%E7%9B%B8%E5%85%B3/kibana2.png)

单击某个字段，会把当前字段数量最多的前5个值及占比显示出来 ：

![kibana3](https://gitlab.com/zhougb3/learning-notes/raw/master/ELK%E7%9B%B8%E5%85%B3/kibana3.png)

点击字段名旁边的`+`，可以将当前值做为一个条件附加到搜索框的搜索条件上 ：

![kibana4](https://gitlab.com/zhougb3/learning-notes/raw/master/ELK%E7%9B%B8%E5%85%B3/kibana4.png)

将鼠标放到刚添加的过滤规则上，会浮现出一排图标，我们可以点击`编辑`，就可以通过完成复杂的语法编写来过滤。

##### Visualize

Visualize是帮助我们通过图表的形式更好地展示搜索得到的结果数据。如下图可以创建一个表格，我们点击`+`：

![kibana5](https://gitlab.com/zhougb3/learning-notes/raw/master/ELK%E7%9B%B8%E5%85%B3/kibana5.png)

之后选择`Data Table`，数据来源我们可以重新搜索，也可以用我们之前保存的搜索结果，这里直接用上面保存的结果`kibana测试`

![kibana6](https://gitlab.com/zhougb3/learning-notes/raw/master/ELK%E7%9B%B8%E5%85%B3/kibana6.png)

![kibana7](https://gitlab.com/zhougb3/learning-notes/raw/master/ELK%E7%9B%B8%E5%85%B3/kibana7.png)

接着点击`Split Rows`，填入信息如下，再点击右上角的三角形按钮，即可得到数据的表格展示。

![kibana8](https://gitlab.com/zhougb3/learning-notes/raw/master/ELK%E7%9B%B8%E5%85%B3/kibana8.png)

![kibana9](https://gitlab.com/zhougb3/learning-notes/raw/master/ELK%E7%9B%B8%E5%85%B3/kibana9.png)



##### DashBoard

Dashboard是对多个图表的联合展示，图表的创建在Visualize中创建保存，create一个新的dashboard再在其中add图表即可。

##### 更多信息可参考：

[kibana教程](http://docs.flycloud.me/docs/ELKStack/kibana/index.html)

[ELK：kibana使用的lucene查询语法](https://segmentfault.com/a/1190000002972420)

[Kibana入门教程](https://www.cnblogs.com/yiwangzhibujian/p/7137546.html)

