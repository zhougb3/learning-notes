# ELK调研报告

### ELK介绍

##### ElasticSearch

- 具备存储海量数据的能力 
- 基于Lucene的分布式索引、查询系统
- 具备一定的数据分析能力

##### LogStash

- 极度灵活的数据收集（input）、解析（filter）、发送(output)通道    

##### Kibana

- 提供丰富数据可视化web平台 
- 查询、和Elasticsearch聚合对应的可视化模块、 Dashboard

### ELK应用场景

可应用于需要进行海量数据存储，数据检索和数据分析的场景。在Stack Overflow和GitHub上都应用其来作数据搜索，ELK还常用于日志数据分析。**以下的调查分析都是面向于日志数据分析的场景**。

### ELK常见架构

1. 各个处于不同服务器的后台服务按照约定的Key写入Redis集群，Logstash从Redis集群中读取日志信息写入ElasticSearch集群。Kibana读取ElasticSearch中的日志，并在Web页面中以表格/图表的形式展示。（Redis集群也可替换为kafka集群）

![工作流1](https://gitlab.com/zhougb3/learning-notes/raw/master/ELK%E7%9B%B8%E5%85%B3/%E6%9E%B6%E6%9E%84%E5%9B%BE1.png)

2. 由Filebeat（Filebeat是本地文件的日志数据采集器，安装在服务器上作为代理来监视日志目录或特定的日志文件）进行各个服务器上日志的采集，再将日志转发到Logstash进行解析，或者转发到中间层Redis，kafka等，再由Logstash去中间层读取。Logstash将日志信息写入ElasticSearch集群。Kibana读取ElasticSearch中的日志，并在Web页面中以表格/图表的形式展示。

![流程图2](https://gitlab.com/zhougb3/learning-notes/raw/master/ELK%E7%9B%B8%E5%85%B3/%E6%9E%B6%E6%9E%84%E5%9B%BE2.jpg)

### ELK测试环境搭建

参考我的另一篇文章：[Docker搭建ELK(Elasticsearch+Logstash+Kibana)分布式日志平台 ](https://gitlab.com/zhougb3/learning-notes/blob/master/ELK%E7%9B%B8%E5%85%B3/docker%E6%90%AD%E5%BB%BAELK(Elasticsearch+Logstash+Kibana)%E7%8E%AF%E5%A2%83%E8%AE%B0%E5%BD%95.md)

### Elasticsearch集群设计原理 

1. Elasticsearch简单的配置信息及说明：

   ```
   cluster.name: es 
   node.name: es1
   path.data: /elk/es/data
   path.logs: /elk/es/logs
   network.host: 192.168.1.31
   http.port: 9200
   transport.tcp.port: 9300
   node.master: true
   node.data: true
   discovery.zen.ping.unicast.hosts: ["192.168.1.31:9300","192.168.1.32:9300"]
   discovery.zen.minimum_master_nodes: 1
   ```

   | 项                                 | 说明                                                         |
   | ---------------------------------- | ------------------------------------------------------------ |
   | `cluster.name`                     | 集群名                                                       |
   | `node.name`                        | 节点名                                                       |
   | path.data                          | 数据保存目录                                                 |
   | path.logs                          | 日志保存目录                                                 |
   | network.host                       | 节点host/ip                                                  |
   | http.port                          | HTTP访问端口                                                 |
   | transport.tcp.port                 | TCP传输端口                                                  |
   | node.master                        | 是否允许作为主节点                                           |
   | node.data                          | 是否保存数据                                                 |
   | discovery.zen.ping.unicast.hosts   | 集群中的主节点的初始列表,当节点(主节点或者数据节点)启动时使用这个列表进行探测 |
   | discovery.zen.minimum_master_nodes | 主节点个数                                                   |

2. Elasticsearch中的节点分为主节点，数据节点，客户端节点。各自的作用如下：

   - 客户端节点：对请求起到负载均衡的作用。将传入的请求路由到集群中的不同节点。  
   - 主节点：主要管理集群信息、primary分片和replica分片信息、维护index信息。例如创建和删除索引， 从集群中添加或删除一台节点等。 master节点无需掌管文档级的变更和索引。这也意味着在只有一台master的情况下，随着负载的增加master不会成为瓶颈。 所有的节点都可能变成master。 
   - 数据节点：存储数据，维护倒排索引，提供数据检索等。 
   - 为了保证主节点稳定，需要将主节点和数据节点分离。如果元信息都在主节点上面，那么主节点挂掉了则该主节点含有的所有Index都无法访问。

3. 脑裂问题：

   1. 如果集群中有节点之间的通讯中断了，可能会出现脑裂的情况。
   2. 将主节点和数据节点分离能够缓解主节点的负载压力，进而能够快速做出响应，减少被误判为主节点宕机的可能性。
   3. 增加discovery.zen.ping_timeout（默认值是3秒） 的值，从而增加节点等待响应时间。
   4. 增加discovery.zen.minimum_master_nodes（默认是1） 的值，官方的推荐值是(N/2)+1，其中N是具有master资格的节点的数量 。
   5. 要尽快发现这个问题，一个比较简单的检测方式是，做一个对每个节点终端响应的定期检查，这个终端返回一个所有集群节点状态的短报告。如果有两个节点报告了不同的集群列表，那么这是一个产生脑裂状况的明显标志。 

4. index与shard：

   - 我们需要创建一个索引index，才可以将数据存储到ES。
   - 在一个index里的每一个文档都属于一个单独的primary shard，所以primary shard的数量决定了你最大能存储的数据量(对应于一个index)。
   - 一个shard实际上是一个Lucene实例，在它的能力范围内拥有完整的搜索功能(在处理它自己拥有的数据时有所有的功能)。我们所有文档的索引indexed(动词)和存储工作都是在shard上， 
   - shard可分为primary shard和replica shard。replica shard用于冗余容灾和提供读请求服务。

5. 当加入一台新机器时，Elasticsearch可以很容易得进行水平扩容，通过均匀分配shared的方式来合理利用每一台机器的性能。当有节点被杀掉时，通过replica shard的作用Elasticsearch也有一定的容灾能力。

6. Elasticsearch集群正常运行情况下集群健康值为green，当集群健康值为yellow时代表主分片全部可用，存在不可用的复制分片。此时的集群是可用的，我们任何请求都能处理，只是所有的操作都落到主分片上，而且可能引发单点故障。当有另一个节点启动后，一切就恢复正常了，主分片的数据会同步到复制分片。（同一个shard的主分片和复制分片不会在同一台机器上）

### Elasticsearch存储及索引原理

##### 存储

1. Elasticsearch是面向文档型数据库，一条数据在这里就是一个文档，用JSON作为文档序列化的格式 。

2. 将Elasticsearch和关系型数据库进行对比：

   ```
   关系数据库     ⇒ 数据库 ⇒ 表    ⇒ 行    ⇒ 列(Columns)
   
   Elasticsearch  ⇒ 索引(Index)   ⇒ 类型(type)  ⇒ 文档(Docments)  ⇒ 字段(Fields)  
   ```

3.  shards文档路由：`shard = hash(routing) % numberofprimary_shards `，这种路由算法强制使primaryshards不可变，因此需要事先对数据规模有所评估来设计可扩展的index。 

4. 每个不同的数据库都有其自身的优点和缺点。Elasticsearch的优点在于其搜索的性能，但是难免会牺牲其他方面的性能，比如插入/更新等。在往Elasticsearch插入一个json的对象时，elasticsearch已经默默为每个字段建立了倒排索引。

##### 索引

1. Elasticsearch分别为每个field都建立了一个倒排索引 ，分为两个字段term和Posting List。要了能够快速查找到term，Elasticsearch对term进行了排序构成Term Dictionary。如果Term Dictionary过大无法全部读入内存，会再建立Term Index，通过term index可以快速地定位到term dictionary的某个offset，然后从这个位置再往后顺序查找。 参考文章：[Elasticsearch－基础介绍及索引原理分析](https://www.cnblogs.com/dreamroute/p/8484457.html)
2. 对于posting list 中的ID，可通过[增量压缩](https://blog.csdn.net/ryfdizuo/article/details/8006551)的方式，Roaring bitmaps来进行存储。
3. 对于联合索引，可以使用[跳跃表](http://blog.jobbole.com/111731/)，对最短的posting list中的每个id，逐个在另外几个posting list中查找看是否存在，最后得到交集的结果。如果使用bitset，直接按位与，得到的结果就是最后的交集。 
4. Elasticsearch的倒排索引，一旦写到磁盘就不会再改变。 Elasticsearch通过新添额外的索引来反映新的更改来替代重写所有倒序索引的方案。每个segment都包含一些提交点(commit point)。新的文档建立时首先在内存建立索引buffer，然后再被写入到磁盘的segment 。每一个提交点都会包含一个.del文件，列举了哪一个segment的哪一个文档已经被删除了。 当一个文档被”删除”了，它仅仅是在.del文件里被标记了一下。 文档更新时，旧版本的文档将会被标记为删除，新版本的文档在新的segment中建立索引。也许新旧版本的文档都会本检索到，但是旧版本的文档会在最终结果返回时被移除。 参考：[ElasticSearch原理简介](https://changsiyuan.github.io/2018/01/18/2018-1-18-ElasticSearch-Intro/)
5. 时间一长，segment的数量会很多。ES通过在后台merge这些segment的方式解决这个问题。小的segment merge到大的，大的merge到更大的。
6. Elasticsearch具备全文检索的能力。它是通过分词组件和语言处理组件等处理来提取term，从而建立倒排索引。参考：[全文检索的原理](https://www.cnblogs.com/forfuture1978/archive/2009/12/14/1623594.html)
7. 更多知识可参考书籍：《elasticsearch 服务器开发》 

### LogStash

Logstash 是一款强大的数据处理工具，它可以实现数据传输，格式处理，格式化输出，还有强大的插件功能，常用于日志处理。 它的基本配置如下：

```
input {
  redis {
    data_type => "list"
    key => "logstash"
    host => "172.17.0.4"
    port => 6379
    threads => 5
    codec => "json"
  }
}
filter {
}
output {
  elasticsearch {
    hosts => ["172.17.0.2:9200","172.17.0.3:9200"]
    index => "logstash-%{type}-%{+YYYY.MM.dd}"
    document_type => "%{type}"
  }
  stdout {
  }
}
```

### kibana

- 数据查询浏览 
-  配置视图（table、柱状图、饼图、线图、大数、地理坐标…）
-  Dashboard    

### 参考

[Elasticsearch 权威指南（中文版）](https://es.xiaoleilu.com/index.html)

[Logstash学习记录--logstash input output filter 插件总结](http://www.voidcn.com/article/p-tpblxuun-bqy.html)

[Kibana入门教程](https://www.cnblogs.com/yiwangzhibujian/p/7137546.html)

[ELK：kibana使用的lucene查询语法](https://segmentfault.com/a/1190000002972420)

[ELK教程](http://docs.flycloud.me/docs/ELKStack/index.html)

[集中式日志系统 ELK 协议栈详解](https://www.ibm.com/developerworks/cn/opensource/os-cn-elk/index.html)

 [基于ELK LinkedIn是如何构建实时实时日志分析系统的 ](http://infor.dezai.cn/Detail.aspx?AI=93609)

[BAT 等一线大厂 Elasticsearch 面试题解读](https://www.jishuwen.com/d/2iSR)

[ELK日志收集系统调研(四) -- 入门学习资源索引](https://blog.csdn.net/iov_aaron/article/details/30227353)

[elasticsearch版本化实现](https://www.elastic.co/guide/cn/elasticsearch/guide/current/optimistic-concurrency-control.html)

[4个最难的 Elastic Search 面试题](https://mp.weixin.qq.com/s/WqyuZCqGt-w2Vn-DVvFKEw)