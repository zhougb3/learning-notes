# Brpc多协议支持实现及协议解析流程浅析

在BIGO公司内部，一个服务可能需要同时支持Brpc协议，Thrift协议，YY协议。幸好，Brpc框架是一个支持协议扩展的框架，具体如何扩展一个协议，[官方文档](https://github.com/apache/incubator-brpc/blob/master/docs/cn/new_protocol.md)已经有详细的说明了。不同协议的区分，Brpc是通过对接收到的字节流中某些特殊字符的识别，或者根据某些语法规则的匹配来实现的 。

Brpc框架对自定义协议的支持，很好地兼容了公司的旧服务，使得旧服务能够很好的与新的Brpc服务做通讯。下面，我们就分别分析一下Brpc协议和YY协议的解析流程。

## Brpc协议解析流程

[官方文档](https://github.com/apache/incubator-brpc/blob/master/docs/cn/baidu_std.md)已经详细讲述了Brpc协议的结构。每个请求包分别包头和包体。

包头：长度固定为12字节。前四字节为协议标识PRPC，中间四字节是一个32位整数，表示包体长度（不包括包头的12字节），最后四字节是一个32位整数，表示包体中的元数据包长度。

包体：分为元数据、数据、附件三部分

我们可以看下`baidu_rpc_meta.proto`中的实现，RpcMeta，RpcRequestMeta ，RpcResponseMeta都支持扩展自定义字段，这样Brpc的使用方可以自由地在这几个message中增加新字段，从而在不同服务间传递一些需要的信息。

这些元数据对框架的使用者来说（业务方），都会封装在controller对象中。

## YY协议解析流程

YY协议头：

```c++
struct Header {
    uint32_t length;
    int32_t uri;
    uint16_t resCode;
};
```

通过协议头的大小拿到整个YY协议体，协议体里面其实也包含了很多元数据（类似from字段这些）拿到之后找到对应的处理函数做处理，处理完之后Answer时会修改一部分from中的字段。

