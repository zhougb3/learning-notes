##### 文本文件VS二进制文件

1. 文件实际上包括两部分，控制信息和内容信息。纯文本文件仅仅是没有控制格式信息罢了，实际上文本文件就是一种特殊的二进制文件（所有的数据类型都是char）
2. 二进制文件是把内存中的数据按其在内存中的存储形式原样输出到磁盘上存放，也就是说存放的是数据的原形式。
3. 文本文件是把数据的终端形式的二进制数据输出到磁盘上存放，也就是说存放的是数据的终端形式。（终端形式也就是每一个都是字符了）
4. 字符数据本身在内存中就经过了编码，所以无论是二进制还是文本形式都是一样的，而对于非字符数据来说，例如inti=10；如果用二进制来进行存储的话为1010，但是如果需要用文本形式来进行存储的话就必须进行格式化编码（对1和0分别编码，即形式为‘1’和‘0’分别对应的码值）。
5. 参考：https://blog.csdn.net/colourful_sky/article/details/80334250

##### 文本协议VS二进制协议

1. 文本协议如json，都是以字符的形式传数据：

   ```
   {
     "code":10,
     "msg":"succeed",
     "info":
     {
        "user":"1234@qq.com",
        "name":"test",
        "group":"test",
        "tel":"18888888"
     }
   }
   ```

   猜测：对于code的值10，其实接收到的也是两个字符，只是因为这里没有`""`，所以一些json的解析包会对它进行转换成对应的某种语言类型。（例如是字符串转int）

2. 二进制协议如protobuf，Protobuf 的消息结构是一系列序列化后的Tag-Value对。其中 Tag 由数据的 field 和 writetype组成，Value 为源数据编码后的二进制数据。 

   ```
   message Person {
     int32 id = 1;
     string name = 2;
   }
   ```

   其中，id字段的field为1，writetype为int32类型对应的序号。编码后id对应的 Tag 为 (field_number << 3) | wire_type = 0000 1000，其中低位的 3 位标识 writetype，其他位标识field。 

   ##### JSON版本化

   始终添加新属性，并且不要删除或重命名现有属性。 例如：

   ```
   {
     "version": "1.0",
     "foo": true
   }
   ```

   如果你想将“foo”属性重命名为“bar”，不要只是重命名它。而应添加一个新属性： 

   ```
   {
     "version": "1.1",
     "foo": true,
     "bar": true
   }
   ```

   由于您从未删除属性，因此基于旧版本的客户端将继续工作。这种方法的缺点是，JSON可能随着时间的推移而变得臃肿，并且您必须继续维护旧的属性。

   ##### protobuf版本化（通过使用可选字段来避免协议的版本号，不需要版本化）

   当原始的protobuf消息改变时，最理想的方式是使用这个消息的各个端都能同步更新。为了让没有更新的端也能够保持正常工作，我们需要做到：

   1. 不可以增加或删除必须(required)字段，这样才能使新旧端都能工作。
   2. 对已经存在的字段的标签数字不可以更改。**protobuf的序列化规则是根据标签号找到对应字段的，如果更改新旧端数字不统一无法找到对应字段。**

   1. 可以删除可选(optional)或重复(repeated)字段，但其序号不可以再被使用。最好是将这个字段重命名，比如加上OBSOLETE_，让后面的开发人员不会再使用该序列号。
   2. 可以添加新的可选或重复字段，但是**必须使用新的标签数字**，必须是之前的字段所没有用过的。 

   注意：

   1. 需要注意的是新的可选消息不会在旧的消息中显示，所以你需要在新的客户端或服务端使用 has_ 严格的检查他们是否存在。
   2. 在 .proto 文件中可以提供一个缺省值。如果没有缺省值，就会有一个类型相关的默认缺省值：对于字符串就是空字符串；对于布尔型则是false；对于数字类型默认为0。
   3. 如果你添加了新的重复字段，你的新客户端或服务端不会告诉你这个字段为空，旧客户端或服务端也不会包含 has_ 标志。
   4. [区分 Protobuf 中缺失值和默认值](https://zhuanlan.zhihu.com/p/46603988)

   ##### protobuf反射实现

   ```
   #include <google/protobuf/descriptor.h>
   #include <google/protobuf/message.h>
   #include <string>
   
   google::protobuf::Message* createMessage(const std::string& type_name)
   {
       google::protobuf::Message* message = NULL;
       const google::protobuf::Descriptor* descriptor =
           google::protobuf::DescriptorPool::generated_pool()->FindMessageTypeByName(type_name);
       if (descriptor)
       {
           const google::protobuf::Message* prototype =
               google::protobuf::MessageFactory::generated_factory()->GetPrototype(descriptor);
           if (prototype)
           {
               message = prototype->New();
           }
       }
       return message;
   }
   ```

   这样使用type_name传参调用DescriptorPool的FindMessageTypeByName拿到一个const类型的Descriptor，然后再通过Descriptor的GetPrototype获得const类型的Message，最后经由const Message的New即可得到一个可自由读写的Message对象。但是需要注意的是这个对象是动态创建的，调用者在使用完毕后必须动态释放掉它，所以推荐使用shared_ptr管理该对象。 

   详细可参考：https://originlee.com/2015/03/14/analysis-google-protobuf-reflection/

   

   