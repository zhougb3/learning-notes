### C++开源协程库libco--原理及应用

##### libco产生背景的理解

1. 原来的微信使用的是半同步半异步的模式（例如：IO线程用epoll，每个新连接给一个线程），接入层为异步模型，业务逻辑层则是同步的多进程或多线程模型 。这样子的并发量收到限制。因此，要提高并发量，无外乎实现全部异步，那么就要改动很多业务代码了。因此，这里使用libco来做协程（协程可以随便开，但是之前的线程不可以，相当于用协程代替之前的线程了）
2. 像muduo网络库，则是完全异步了。你不能在IO线程做耗时或阻塞操作。如果有耗时操作则放在线程池的工作线程中（个人觉得在工作线程也不好用阻塞，毕竟线程资源毕竟宝贵，阻塞的话要是线程池满了呢，阻塞的操作比如说读文件，发起一个http请求等，这里是不是可以说注册一个回调，当http请求返回之后调用某个处理函数呢）。所以用muduo做开发在哪里都要用异步。
3. 所以如果我们用muduo，然后在工作线程做阻塞开发，那么结果可能跟微信一样，要不改动代码提高并发量就要用协程了。

##### 笔记

1. libco是一种非对称协程。非对称协程（asymmetric coroutines）是跟一个特定的调用者绑定的，协程让出 CPU 时，只能让回给原调用者。caller 与 callee 关系是确定的，不可更改的，非对称协程只能返回最初调用它的协程。

2. 对称协程机制一般需要一个调度器的支持，按一定调度算法去选择 yield 的目标协程。Go 语言提供的协程，其实就是典型的对称协程。不但对称， goroutines 还可以在多个线程上迁移。

3. libco 内部还为保存协程的调用链留了一个 stack 结构，而这个 stack 大小只有 固定的 128。使用 libco，如果不断地在一个协程运行过程中启动另一个协程，随着嵌套深度增加就可能会造成这个栈空间溢出。 

4. 在 OS 之上实现用户级线程（协程）。跟 OS 线程一样，用户级线程也应该具备这三个要素（有程序，有私有空间，有唯一标志）。所不同的只是第二点，用户级线程 （协程）没有自己专属的堆空间，只有栈空间。    

5. libco 的协程一旦创建之后便跟创建时 的那个线程绑定了的，是不支持在不同线程间迁移（migrate）的。

6. 协程分为stackful 协程和stackless 协程 。stackful 协程有两种技术：Separate coroutine stacks 和 Copying the stack （又叫共享栈）    

7. 这里的栈内存，无论是使用预先分配的共享栈，还是 co_create 内部单独分配的栈，其实都是调用 malloc 从进程的堆内存分配出来的。对于协程而言，这就是“栈”，而对于底层的进程（线程）来说这只不过是普通的堆内存而已。

8. co_start 只有一次，而 co_resume 可以是暂停之后恢复启动，可 以多次调用，就这么个区别。    

9. co_resume() 函数内部会调用 coctx_swap() 将当前协程挂起，因此这其实是一个串行的过程。

10. 我们调用 co_create()、 co_resume() 启动协程执行一次性任务，当任 务结束后要记得调用 co_free() 或 co_release() 销毁这个临时性的协程，否则将引起内存泄漏    

11. libco 协程库在单个线程中实现了多个协程的创建和切换。 

12. libco使用时间轮做定时器管理，1毫秒1次

13. 使用 epoll 加时间轮的实现定时器的算法如下： 
    1. Step 1 [epoll_wait] 调用 epoll_wait() 等待 I/O 就绪事件，最⼤等待时长设置为 1 毫 秒（即 epoll_wait() 的第 4 个参数）。 
    2. Step 2 [处理 I/O 就绪事件] 循环处理 epoll_wait() 得到的 I/O 就绪⽂件描述符。 
    3. Step 3 [从时间轮取超时事件] 从时间轮取超时事件，放到 timeout 队列。 
    4. Step 4 [处理超时事件] 如果 Step 3 取到的超时事件不为空，那么循环处理 timeout 队列中的定时任务。否则跳转到 Step 1 继续事件循环。 
    5. Step 5 [继续循环] 跳转到 Step 1，继续事件循环。    

14. 先来看 yield，实际上在 libco 中共有 3 种调用 yield 的场景： 1. 用户程序中主动调用 co_yield_ct()； 2. 程序调用了 poll() 或 co_cond_timedwait() 陷⼊“阻塞”等待； 3. 程序调用了 connect(), read(), write(), recv(), send() 等系统调用陷⼊“阻塞”等待。 相应地，重新 resume 启动一个协程也有 3 种情况： 1. 对应用户程序主动 yield 的情况，这种情况也有赖于用户程序主动将协程 co_resume() 起来； 2. poll() 的目标⽂件描述符事件就绪或超时， co_cond_timedwait() 等到了其他协程 的 co_cond_signal() 通知信号或等待超时； 3. read(), write() 等 I/O 接⼝成功读到或写⼊数据，或者读写超时。    

15. 调用 poll() 进入了“阻塞”等待，等待 1 秒钟。这个 poll 函数内部实际上做了两件事。首先，将自己作为一个定时事件注册到当前执行环境 的定时器，注册的时候设置了 1 秒钟的超时时间和一个回调函数（仍是一个用于未来 “唤醒”自己的回调）。然后，就调用 co_yield_env() 将 CPU 让给主协程了。    

16. 用hook方法重写写了read等方法，使得阻塞调用read能够得到非阻塞的性能。

17. co_eventloop的运行过程：

    第 6 ⾏：调用 epoll_wait() 等待 I/O 就绪事件，为了配合时间轮⼯作，这里的 timeout 设置为 1 毫秒。 第 8~10 ⾏： active 指针指向当前执⾏环境的 pstActiveList 队列，注意这里面可能已 经有“活跃”的待处理事件。 timeout 指针指向 pstTimeoutList 列表，其实这个 timeout 完 全是个临时性的链表， pstTimeoutList 永远为空。 第 12~19 ⾏：处理就绪的⽂件描述符。如果用户设置了预处理回调，则调用 pfnPrepare 做预处理（15 ⾏）；否则直接将就绪事件 item 加⼊ active 队列。实际上， pfnPrepare() 预处理函数内部也是会将就绪 item 加⼊active 队列，最终都是加⼊到 active 队列等到 32~40 ⾏统⼀处理。 第 21~22 ⾏：从时间轮上取出已超时的事件，放到 timeout 队列。 第 24~28 ⾏：遍历 timeout 队列，设置事件已超时标志（bTimeout 设为 true）。 第 30 ⾏：将 timeout 队列中事件合并到 active 队列。 第 32~40 ⾏：遍历 active 队列，调用⼯作协程设置的 pfnProcess() 回调函数 resume 挂起的⼯作协程，处理对应的 I/O 或超时事件。    

18. 协程的切换：`void coctx_swap(coctx_t* curr, coctx_t* pending) asm("coctx_swap");    `coctx_swap 接受两个参数，无返回值。其中，第一个参数 curr 为当前协程的 coctx_t 结构指针，其实是个输出参数，函数调用过程中会将当前协程的 context 保存在这个参 数指向的内存里；第二个参数 pending，即待切入的协程的 coctx_t 指针，是个输入参数， coctx_swap 从这里取上次保存的 context，恢复各寄存器的值。前面我们讲过 coctx_t 结 构，就是用于保存各寄存器值（context）的。这个函数奇特之处，在于调用之前还处于 第一个协程的环境，该函数返回后，则当前运行的协程就已经完全是第二个协程了。    

19. [libco的介绍](https://www.infoq.cn/article/CplusStyleCorourtine-At-Wechat)

20. [libco源码分析](https://github.com/hymanyx/libco)

21. [libco源码](https://github.com/Tencent/libco)

22. [libco协程库上下文切换原理详解](https://blog.csdn.net/lqt641/article/details/73287231)