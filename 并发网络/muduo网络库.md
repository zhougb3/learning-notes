##### 类库和框架的区别：

类库是类的集合，这些类之间可能是相互独立的。应用开发者希望使用任何一个类时可以直接调用它，而不必再写一个。与类库相比，框架和类库有着相似的形式，即框架也往往是类的集合；但不同之处在于，框架中的各个类并不是孤立的，而框架中的业务逻辑代码是将不同的类“连”在一起，在它们之间建立协作关系。

##### make，cmake，autotool，scons区别

1. make工具可以看成是一个智能的批处理工具，它本身并没有编译和链接的功能，而是用类似于批处理的方式—通过调用makefile文件中用户指定的命令来进行编译和链接的。 
2. cmake可以更加简单的生成makefile文件给上面那个make用。cmake还可以跨平台生成对应平台能用的makefile，你不用再自己去修改了。 要根据一个叫CMakeLists.txt文件（学名：组态档）去生成makefile。 CMakeLists.txt还是需要你手写。
3. autotool的目的也是为了方便你写Makefile。
4. Scons是一个开放源码、以Python语言编码的自动化构建工具，可用来替代make编写复杂的makefile。并且scons是跨平台的，只要scons脚本写的好，可以在Linux和Windows下随意编译。
5. 基于scons腾讯开发了一个blade的构建工具，具体参考：https://www.cnblogs.com/chen3feng/p/4075131.html

##### muduo运行环境docker搭建

1. 创建docker：

   ```
   docker run -it --name muduo_ubuntu -v C:\Users\zhoug\Desktop\muduo-master:/home/muduo --net=host ubuntu:16.04 /bin/bash
   ```

2. 安装环境

   ```
   apt-get update
   apt-get install make
   apt-get install cmake（总是报hashsum错，但是多试几次就可以了，好像是中间换了网络？）
   apt-get install g++
   apt-get install libboost-dev libboost-test-dev
   apt-get install libcurl4-openssl-dev libc-ares-dev
   apt-get install protobuf-compiler libprotobuf-dev
   apt-get install curl
   apt-get install vim
   apt-get install net-tools
   ```

3. 编译安装muduo库

   ```
   ./build.sh -j2
   ./build.sh install
   ```

4. 运行测试案例：

   ```
   bin/inspector_test
   ```

5. 运行测试案例之后会监听12345端口，要在宿主主机上通过`http://localhost:12345`访问服务是访问不到的，因为docker的端口是和Linux虚拟主机绑定的，而不是和宿主主机绑定的。Hyper V  会给虚拟主机一个IP地址。通过在docker上运行`ipconfig`可以找到地址，在宿主主机上访问`http://10.0.75.2:12345`可以成功访问。

   ```
   hvint0    Link encap:Ethernet  HWaddr 00:15:5d:0c:30:05
             inet addr:10.0.75.2  Bcast:0.0.0.0  Mask:255.255.255.0
             inet6 addr: fe80::215:5dff:fe0c:3005/64 Scope:Link
             UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
             RX packets:341 errors:0 dropped:0 overruns:0 frame:0
             TX packets:155 errors:0 dropped:0 overruns:0 carrier:0
             collisions:0 txqueuelen:1000
             RX bytes:42205 (42.2 KB)  TX bytes:15174 (15.1 KB)
   ```

6. 网上说在`Kitematic `也可以看到IP和端口，但是我下载之后还是没有找到。

7. 可以参考这个写个dockerfile：https://gist.github.com/alext234/5d84fbaecafd1d9d2249e5e2d9610910

##### 并发和并行

使用单线程reactor可以实现并发但不可以实现并行。

##### 信号处理

在多线程中处理信号是个相当麻烦的事情，应当尽量避免。

[SIGPIPE信号](https://www.cnblogs.com/caosiyang/archive/2012/07/19/2599071.html) ：连接建立，若某一端关闭连接，而另一端仍然向它写数据，第一次写数据后会收到RST响应，此后再写数据，内核将向进程发出SIGPIPE信号，通知进程此连接已经断开。而SIGPIPE信号的默认处理是终止程序 。

[https://www.cnblogs.com/cobbliu/p/5592659.html](https://www.cnblogs.com/cobbliu/p/5592659.html)

##### eventfd

1. eventfd是用来实现多进程或多线程的之间的事件通知的 
2. 与 条件变量相比，eventfd为I/O事件驱动，能和服务器的I/O模式很好的融合。
3. 与管道相比，管道多用了一个文件描述符，而且管道的话内核还得给其管理的缓冲区，eventfd则不需要，所以单纯作为事件通知的话还是eventfd好用 
4.  `int eventfd(unsigned int initval, int flags); `创建一个文件描述符 
5. 参考：https://blog.csdn.net/Shreck66/article/details/49757685

##### __thread关键字

1. __thread变量每一个线程有一份独立实体，各个线程的值互不干扰。可以用来修饰那些带有全局性且值可能变，但是又不值得用全局变量保护的变量。 
2. 参考：https://blog.csdn.net/liuxuejiang158blog/article/details/14100897

##### muduo解读

1. 在发送数据时，如果客户端从不接收数据或者数据接收很慢，那我们的内存很可能就会爆满。解决方法如下：关注onwritecomplete事件，当缓冲区全部发完之后再写入新数据。或者缓冲区累积到一定大小主动断开连接。
2. 多个服务端和多个客户端可以共享一个eventloop对象。
3. muduo在主动关闭连接的时候只会调用shutdown，保证了数据读取的完整性。在被动关闭的时候读到0就会close连接。

   

#####  限制并发连接数

文件描述符的数量是有限的，可以通过设置一个低一点的soft limit，如果超过这个数量就主动关闭新连接。也可以先预留一个空闲描述符，当新连接没办法获得描述符时就将这个预留描述符用上并马上断开连接。

##### timewheel（时间轮盘）踢掉空闲连接

1. 设置一个定时器，每次定时添加一个新桶（循环队列，会删除队列的第一个桶），这样子删除桶内用shareptr管理的TCPConnection的引用计数减去1，如果该连接不在其余桶内则会超时被析构。
2. 每次有新连接或者新消息到达则在队列的最后一个桶内的set中记录本连接。

##### 消息广播服务（发布-订阅机制）

跑一个hub的服务进程，再写一个客户端库。用map，set容器来存储发布者，订阅者信息

##### http服务部分

---

### muduo并发模型选择

##### 多线程VS多进程

1. 可以更高效地共享数据，但是线程之间同步和加锁控制比较麻烦，同时也会影响性能
2. 与多进程相比，多线程方式消耗的总资源比进程方式好，但是因每个线程与主程序共用地址空间而受限于2GB地址空间，同时一个线程的崩溃可能影响到整个程序的稳定性，一个进程内的线程数量也是有限的
3. 多进程可以运行于多台计算机上，而多线程则运行于一台多核计算机上。进程不能跨OS边界
4. **多进程+多线程的服务程序是被鄙弃的，因为他结合了二者的缺点**

##### 协程

1. 协程是一种用户态的轻量级线程，是用户级别的，对内核是不可见的
2. 为了充分挖掘单核CPU的性能，我们采用了基于事件驱动的异步编程模式，其特点是异步+回调。而后来，为了能够用看似同步的编程方式达到原来异步+回调的效果，就出现了协程。它是实现推拉互动的所谓非抢占式协作的关键。
3. 由于用传统的异步+回调方式程序员可能会在某些地方忘记异步操作，在这种场景下使用协程的性能就更好了。 
4.  IO密集型应用: 多进程->多线程->事件驱动->协程     CPU密集型应用:多进程-->多线程 
5. 在使用协程的编程时，一般采用的是多进程+多协程，当然也可以多线程+多协程（可能由于协程已经很好的利用单核CPU的性能，因此采用多进程更简单更安全一些）

##### 作者推荐的多线程服务端编程模式

1. one loop per thread + thread pool
2. thread pool 用来做计算。具体可以是任务队列和生产者消费者队列：任务队列放的是函数对象，生产者消费者队列放的是数据

##### 必须用单线程的情况

1. 程序可能会fork()。fork只克隆了当前线程，而且会有其他问题。唯一安全的做法是fork后马上exec另一个程序
2. 限制程序的CPU占用率

##### 多线程程序没有优势的情况

1. IO瓶颈的应用：如果一个单线程的程序就能撑满IO，那么多线程就没有用了
2. CPU瓶颈的应用：大部分时间都用与计算，那么多进程程序就可以了，实现上更加简单。
3. 多线程的适用场景是提高响应速度，让IO和计算相互重叠，降低延迟（首次请求，通过并行计算可能可以提高响应速度）。虽然多线程不能提高绝对性能，但能提高平均响应性能。

##### 各种并发编程模型的对比

![1](https://gitlab.com/zhougb3/learning-notes/raw/master/images/1.png)

总结：

1. 如果不使用IO多路复用，使用一个连接一个线程或进程的方式，那么并发数不高
2. 使用IO多路复用，并且为每个请求创建一个线程处理，这样子会增加频繁创建线程的开销，因此可以用线程池的方式。线程池的另外一个作用还可以用来进行阻塞操作（比如有些数据库的客户端只提供同步访问，有些IO函数没有非阻塞的版本）
3. 如果IO压力过大，一个reactor处理不过来，则可以用one loop per thread 的方法，这样还可以保证请求的顺序性，也减少了进出线程池的上下文切换。小规模的计算可以在当前IO线程完成并返回结果，从而降低响应的延迟。
4. 如果各个连接之间没有交互，也可以用one loop per process。
5. 最灵活的方式就是多个reactor+线程池，可以解决突发计算的问题。

### muduo定时器实现

##### Linux下定时器的实现

1. 2.4版本内核下：使用`setitimer `接口，由于 `setitimer() `不支持在同一进程中同时使用多次以支持多个定时器，因此，如果需要同时支持多个定时实例的话，需要由实现者来管理所有的实例。
2. 2.6版本内核下：使用`timer_create `接口，POSIX timer接口支持在一个进程中同时拥有多个定时器实例，所以在上面的基于 setitimer() 和链表的 PerTickBookkeeping 动作就交由 Linux 内核来维护，这大大减轻了实现定时器的负担。 
3. 2.6版本内核下提供了基于文件描述符的相关定时器接口 `timerfd_create `，可以创建多个定时器，但是要考虑到文件描述符的个数有限。
4. 参考：[Linux 下定时器的实现方式分析](https://www.ibm.com/developerworks/cn/linux/l-cn-timers/index.html)

##### 用户层定时器管理

1. 排序链表，按到期时间排序
2. 最小堆的方法，堆顶为到期时间最短的定时器
3. 时间轮方式（简单时间轮和类似水表的多度量范围时间轮）：每个周期检查一次
4. 二叉搜索树（muduo就直接使用了set容器，本质就是红黑树，Nginx就是用了红黑树）

##### 各种定时器管理的复杂度

![2](https://gitlab.com/zhougb3/learning-notes/raw/master/images/2.jpg)

##### muduo定时器实现流程

1. 每个线程都有一个EventLoop对象，EventLoop对象初始化的时候会new一个TimerQueue，因此每个IO线程会有一个TimerQueue，TimerQueue对象初始化的时候就会分配好Timerfd和channel等，当Timerfd有可读事件发生时，就会从队列中拿出到时事件处理
2. 在增加和删除定时器时，通过**runInLoop**方法保证了能够在非IO线程给指定EventLoop的IO线程增加和删除定时器。因此，保存定时器的队列也不需要加锁
3. TimerQueue有一个`cancelingTimers_ `成员，其作用是：当在定时队列中找不到删除的定时器，而此时正在执行定时器的回调函数，那么就将此定时器记录下来。在此次所有的定时器执行完成之后会调用reset 函数，如果是个周期性的定时器且不在`cancelingTimers_ `中，则可以将其重新加入我们的TimerQueue中。
4. 在reset函数中，如果不是周期性定时器就会被delete掉，在cancel函数和TimerQueue析构函数中都会delete掉Timer对象。（可用unique_ptr解决这个问题，使用`std:move `应该能够实现转移）
5. runInLoop的实现中调用了queueinloop方法，该方法会往目标IO线程的EventLoop对象的pendingFunctors_增加元素（`typedef std::function<void()> Functor `参数为空，所有返回值类型都兼容），同时唤醒该线程（eventfd可读），而在EventLoop的循环中就会定期执行这些函数。 

### muduo Buffer类设计

1. 输入输出都需要缓冲，输入的缓冲可以不会重复触发可读事件，等待构成一条完整的消息再通知业务逻辑。输出缓冲可以保证应用程序的输出不会阻塞，网络库的缓冲会保证在可写时把缓冲内容发送出去。
2. Buffer的存储形式为`vector<char> buffer_ `，Buffer类使用了iovec来利用临时栈上的空间以减轻为每个连接分配的缓冲区大小，同时也避免了重复调用read的系统开销。
3. Buffer类不是线程安全的，因为对于InputBuffer的接收onMessage只在IO线程调用，对于outputBuffer的写只在TcpConnection::send中调用，该函数是线程安全的。
4. Buffer初始化后如下图：

![1](https://gitlab.com/zhougb3/learning-notes/raw/master/images/1.jpg)

5. 在写入和读取的过程中，两个index会向右移动，当它们再次重复在一起的时候，两个index又会回到8这个位置。当size已经不满足写入的时候会自动增长，读完之后大小不会缩小。当size满足写入大小但内存不连续时会自动发生内存挪移，readindex回到8，之后再写入。prepend的8个字节大小是因为我们经常在发送消息时要加上消息的大小，通过预留的方式以空间换时间。
6. buffer在头部预留了8个字节的空间，因此prepend操作就不需要移动已有的数据，效率较高。

### muduo运行流程

1. channel对象负责fd的IO事件分发，调用相关的回调函数，并不拥有fd。channel记录了其对应的fd以及它在pollfd集合的下标。

2. PollPoller 类主要存储了pollfd的集合和pollfd和channel对象之间的map映射关系，可以增删改查channel对象对应的pollfd（注意没有增删改查channel），并返回有指定事件发生的channel对象的指针集合。如果不关心一个文件描述符的任何事件，我们会将其文件描述符改为相反数减去1（目的是不用监控任何事件，且在真正删除channel对应的pollfd时，我们是让目标pollfd和pollfd集合最后一个元素再去除最后一个元素，然后再将原本最后一个元素对应的channel存储的下标值更改为新的下标）。参考：https://blog.csdn.net/q5707802/article/details/79325224

3. EventLoop的事件循环过程：调用poll函数得到活跃的channel，再依次调用channel的handleEvent事件，之后再调用其他线程指定本线程调用的回调函数（runinloop）

4. TcpServer建立连接流程：新连接到达后会创建一个TcpConnection对象，设置好他的回调，再调用连接建立的回调函数。

   ![3](https://gitlab.com/zhougb3/learning-notes/raw/master/images/3.jpg)

5.  用户给TcpServer设置回调，TcpServer给TcpConnection设置回调，TcpConnection给channel设置回调（但是不是同个回调了，比如TcpConnection给channel设置的可读回调会检测读到的是否为0，不是0才调用TcpServer给的消息回调）

6. TcpConnection拥有TCP socket，它的析构函数会close（fd）

7. TcpServer销毁连接流程：在TcpConnection的handleClose函数中先执行用户提供的connectionCallback_ 函数表明要断了连接，然后调用TcpServer注册的closeCallback_ 函数去移除TcpServer对其的引用计数，同时通过queueInLoop 去延迟调用TcpConnection的connectDestroyed 函数，通过bind保证了TcpConnection至少还有一个引用计数，这时候channel的handleRead函数算是结束了。最后，调用connectDestroyed 函数时就去删除channel对应的pollfd，然后如果用户没有对TcpConnection的引用，此时TcpConnection就销毁了。（要通过queueinloop的方式是因为不可以在channel的handleRead中销毁channel）

![4](https://gitlab.com/zhougb3/learning-notes/raw/master/images/4.jpg)

8. 多线程的TcpServer：通过使用EventLoopThreadPool ，然后在新建连接时通过getNextLoop 方法得到loop对象去初始化连接即可，而在删除连接时通过runinloop方式将ioloop的调用转移到TcpSerever的loop_线程上来，最后还会再次将connectDestroyed 转移到ioloop线程上来。

### muduo中用protobuf实现RPC调用

1. 我们可以通过protobuf service 使用rpc调用
2. 关键是实现了类`RpcChannel : public ::google::protobuf::RpcChannel `定义一个RpcMessage的message，客户端在通过stub进行rpc调用时，实际上是封装成了一个RpcMessage对象发送给服务端，服务端接收到消息后调用onRpcMessage 方法，如果是响应消息，则调用注册的处理函数，如果是请求消息，则调用相关注册的服务方法后将结果封装成RpcMessage对象返回给客户端。

```
    message RpcMessage
    {
      required MessageType type = 1;
      required fixed64 id = 2;

      optional string service = 3;
      optional string method = 4;
      optional bytes request = 5;

      optional bytes response = 6;

      optional ErrorCode error = 7;
    }
```

3. 我们还需要一个protobuf的解编码器（定义一个传输格式），还有protobuf的消息分发器（不同的消息类型调用不同的注册函数）
4. 参考：[基于protobuf service使用rpc入门教程](https://blog.csdn.net/NK_test/article/details/72682780)
5. **有个问题：在通过stub调用远端服务时，会不会阻塞在网络发送上呢，这得看protobuf的实现了**

### 一个类似muduo的网络库

[Sinetlib: A High Perfermance C++ Network Library](https://github.com/silence1772/Sinetlib)