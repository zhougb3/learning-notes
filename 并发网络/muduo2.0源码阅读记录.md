### muduo2.0源码阅读记录

花了20天的时间读了陈硕先生的《Linux多线程服务端编程》一书的前8章。当然，每天阅读的时间并不算多，中间有些部分也反反复复看了几遍，最后也算是能勉强接受作者传授的知识。配合书把muduo2.0网络部分的代码和日志库代码细读了一遍，这也算是个人第一次较为深入地去读取一个开源项目源码。通过书和源码的阅读，确实是对不少东西加深了理解。

本来想按自己的理解来写源码阅读笔记的，但考虑到网上关于muduo代码的解析文章已经很多并且写的很好了，就放弃了这个想法。摘录几个自己在源码阅读过程中参考的网页：

[muduo网络库学习](https://blog.csdn.net/Simba888888/column/info/leanmuduo)

[muduo学习](https://blog.csdn.net/q5707802/article/category/7443069)

[muduo定时器](http://www.voidcn.com/article/p-pejniosj-bd.html)

[muduo定时器](https://blog.csdn.net/nk_test/article/details/51057879)

