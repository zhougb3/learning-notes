# Redis深度历险：核心原理与应用实践

### Redis基础数据结构

1. Redis 有 5 种基础数据结构，分别为： string (字符串)、 list (列表)、 set (集合)、 hash (字典) 和 zset (有序集合)。value值可以是个整数，进行自增。
2. Redis 所有的数据结构都是以唯一的 key 字符串作为名称，然后通过这个唯一 key 值来获取相应的 value 数据。不同类型的数据结构的差异就在于 value 的结构不一样 。
3. Redis 的字符串是动态字符串，是可以修改的字符串，内部结构采用预分配冗余空间的方式来减少内存的频繁分配。
4. 可以对 key 设置过期时间，到点自动删除，这个功能常用来控制缓存的失效时间。    
5. Redis的列表底层是链表不是数组，插入删除复杂度为O(1) ，但索引很慢为O(n)。
6. 列表的lindex需要对链表进行遍历，性能随着参数 index 增大而变差。
7. Redis对于列表的底层实现其实是当数据量较小时用一块连续内存ziplist存储，当数据量较大时用双向指针连接多个ziplist构成quicklist。
8. Redis的字典的值只能是字符串（**Redis字符串键是可以存储数字的**，并且当存储是数字的时候，Redis有一些命令可以专门处理这种数字的值）。Redis采用的是渐近式的rehash策略：保留新旧两个 hash 结构，查询时会同时查询两个 hash 结构，然后在后续的定时任务中以及 hash 的子指令中，循序渐进地将旧 hash 的内容 一点点迁移到新的 hash 结构中。当 hash 移除了最后一个元素之后，该数据结构自动被删除，内存被回收。
9. Redis的Set内部的键值对是无序的唯一的。它的内部实现相当于一个特殊的字典，字典中所有的 value 都是一个值 NULL。
10. zset一方面它是一个 set，保证了内部 value 的唯一性，另一方面它可以给每个 value 赋予一个 score，代表这个 value 的排序权重。它的内部实现用的是一种叫着「跳跃列表」的数据结构。    
11. 如果一个字符串（key）已经设置了过期时间，然后你调用了 set 方法修改了它，它的过期时间会消失 。

### 分布式锁

##### 本地锁和分布式锁

1. 本地锁是用于单进程多线程的条件下。加一个锁来保证同一时刻只有一个线程能操作某个进程的共享资源。
2. 在分布式环境的多个进程下，由于不在统一机器的多个进程无法共享资源，因此我们需要用其他方法来控制对全局资源的并发修改。这个问题的解决就是将锁放入所有进程都可以访问的地方，比如数据库，redis，memcached或者是zookeeper。这些也是目前实现分布式锁的主要实现方式。 
3. 可参考：[锁和分布式锁](https://blog.csdn.net/zhanglh046/article/details/78630371)

 ##### Redis分布式锁

1. 通过set指令和expire的联合指令`set lock:codehole true ex 5 nx OK ... do something critical ... > del lock:codehole `来实现，但是redis分布式锁不要用于较长时间的任务。
2. Redis 的分布式锁不能解决超时问题，如果在加锁和释放锁之间的逻辑执行的太长，以至 于超出了锁的超时限制，就会出现问题。因为这时候锁过期了，第二个线程重新持有了这把锁， 但是紧接着第一个线程执行完了业务逻辑，就把锁给释放了，第三个线程就会在第二个线程逻辑执行完之间拿到了锁。
3. 可以实现可重入锁，但是没必要。（可重入性是指线程在持有锁的情况下再次请求加锁，如果一个锁支持同一个线程的多次加 锁，那么这个锁就是可重入的）
4. 参考：[基于 redis 的分布式锁实现](https://mp.weixin.qq.com/s/DL-d9V69paxN77V6V1PwXw)，这里面让每个线程set的value值具有唯一性（IP+PID+线程ID），保证不要误删其他线程的锁，不会出现递归误删锁的情况。但是还是不能彻底解决问题。因此还是要把锁的过期时间设置的足够合理。（Lua脚本能够保证get和set两个操作执行的原子性）
5. 分布式锁是一种悲观锁    

### 延时队列

1. Redis 的消息队列不是专业的消息队列，它没有非常多的高级特性， 没有 ack 保证，如果对消息的可靠性有着极致的追求，那么它就不适合使用。
2. Redis 的 list(列表) 数据结构常用来作为异步消息队列使用 。客户端在取数据的时候可以使用blpop/brpop来阻塞读，有效避免取不到时空转轮询和拉高Redis的QPS。但是Redis的空闲连接会自动断开，这时候blpop/brpop会抛出异常来，注意捕获异常。    
3.  延时队列可以通过 Redis 的 zset(有序列表) 来实现。我们将消息序列化成一个字符串作 为 zset 的 value，这个消息的到期处理时间作为 score，然后用多个线程轮询 zset 获取到期的任务进行处理，多个线程是为了保障可用性，万一挂了一个线程还有其它线程可以继续处理。因为有多个线程，所以需要考虑并发争抢任务，确保任务不能被多次执行。（zrangebyscore 和 zrem 是两个不同的操作，先看能不能拿到数据，能再拿出数据）    

### 节衣缩食 —— 位图    

Redis 提供了位图数据结构。

### 四两拨千斤 —— HyperLogLog    

统计一个网站每天的UV（访客数）：

1. 为每一个页面一个独立的 set 集合来存储所 有当天访问过此页面的用户 ID。当一个请求过来时，我们使用 sadd 将用户 ID 塞进去就可 以了。通过 scard 可以取出这个集合的大小，这个数字就是这个页面的 UV 数据。
2. 访问量超级大时会浪费很多存储空间。
3. HyperLogLog 数据结构就是用来解决 这种统计问题的。 HyperLogLog 提供不精确的去重计数方案，虽然不精确但是也不是非常不 精确，标准误差是 0.81%，这样的精确度已经可以满足上面的 UV 统计需求了 。

### 层峦叠嶂 —— 布隆过滤器    

1. 当需要查找一个值是否在某个集合中时，HyperLogLog则没有这个功能，这时候就需要布隆过滤器了。
2. 当布隆过滤器说某个值存在时，这个值可能不存在；当它说不存在时，那就肯定不存在。
3. 布隆过滤器原理

### 断尾求生 —— 简单限流    

1. 作用：限定用户的某个行为在指定的时间里只能允许发生 N 次 。
2. 使用zset，利用score值来实现。
3. 缺点：它要记录时间窗口内所有的行为记录，如果这 个量很大，比如限定 60s 内操作不得超过 100w 次这样的参数，它是不适合做这样的限流的，因为会消耗大量的存储空间。

### 应用 7：一毛不拔 —— 漏斗限流    

提供了容量限制。

### 应用 8：近水楼台 —— GeoHash    

1. 数据库中记录每个人的ID和经纬度，要得到附近的人，首先`select id from positions where x0-r < x < x0+r and y0-r < y < y0+r  `得出一个矩形，再对矩形内的人进行求距离排序。如果搜索不到可以继续扩大半径r。
2. Redis中有内置的GeoHash算法用来存和计算距离。

### 应用 9：大海捞针 —— Scan    

1. 查找符合某个规则的key，可以使用keys指令，提供一个简单的正则字符串就可以。但是具有以下缺点：
   1. 没有 offset、 limit 参数  ，当结果值过多会刷屏。
   2. keys指令是简单的遍历算法，复杂度为O(N)，当有千万级以上的key时会导致服务卡顿（redis是单线程服务）
2. 使用scan指令来解决这个问题，它是通过游标分步进行的，不会阻塞线程;
3. Redis中所有的key都存储在一个字典中（类似于hashmap）
4. scan采用的是高位进位加法：高位进位法从左边加，进位往右边移动，同普通加法正好相反。但是最终它们都会遍历所有的槽位并且没有重复 。采用这种加法的目的是在扩容的时候能够尽量保证遍历不重复
5. Redis采用的是渐进式rehash。这意味着要操作处于 rehash 中的字典，需要同时访问新旧两个数组结构。如果在旧数组下面找不到元素，还需要去新数组下面去寻找。
6. 在平时的业务开发中，要尽量避免大 key 的产生。否则内存的申请和回收都会产生卡顿的现象。

### 原理 1：鞭辟入里 —— 线程 IO 模型    

1. Redis 是个单线程程序！    
2. Redis 会将每个客户端套接字都关联一个指令队列。客户端的指令通过队列来排队进行顺序处理，先到先服务。
3. Redis 同样也会为每个客户端套接字关联一个响应队列。 Redis 服务器通过响应队列来将 指令的返回结果回复给客户端。
4. Redis将定时任务记录在一个称为最小堆的数据结构中。将最快要执行的任务还需要的时间作为select 系统调 用的 timeout 参数。这与Nginx 和 Node 的事件处理原理类似。

### 原理 2：交头接耳 —— 通信协议    

1. 客户端向服务器发送的指令只有一种格式，多行字符串数组    
2. RESP(Redis Serialization Protocol) 是一种直观的文本协议，优势在于实现异常简单，解析性能极好 
3. 数据协议是肯定要的，因为TCP是一种流协议，客户端和服务端得根据定好的协议才知道发送是否完成了。

### 原理 3：未雨绸缪 —— 持久化    

1. Redis 的持久化机制有两种，第一种是快照，第二种是 AOF 日志。快照是一次全量备份， AOF 日志是连续的增量备份。
2. Redis 还需要进行内存快照，内存快照要求 Redis 必须进行文 件 IO 操作，可文件 IO 操作是不能使用多路复用 API。Redis 使用操作系统的多进程 COW(Copy On Write) 机制来实现快照持久化 。
3. COW:当父进程对其中一个页面的数据进行修改时，会将被共享的页面复 制一份分离出来，然后对这个复制的页面进行修改。这时子进程相应的页面是没有变化的， 还是进程产生时那一瞬间的数据。    
4. 假设 AOF 日志记录了自 Redis 实例创建以来所有的修改性指令序列，那么就可以通过 对一个空的 Redis 实例顺序执行所有的指令，也就是「重放」，来恢复 Redis 当前实例的内 存数据结构的状态。    
5. Redis 会在收到客户端修改指令后，先进行参数校验，如果没问题，就立即将该指令文 本存储到 AOF 日志中，也就是先存到磁盘，然后再执行指令。    AOF 日志需要瘦身 
6. Redis 提供了 bgrewriteaof 指令用于对 AOF 日志进行瘦身。其原理就是开辟一个子进 程对内存进行遍历转换成一系列 Redis 的操作指令，序列化到一个新的 AOF 日志文件中。 序列化完毕后再将操作期间发生的增量 AOF 日志追加到这个新的 AOF 日志文件中，追加 完毕后就立即替代旧的 AOF 日志文件了，瘦身工作就完成了。    
7. AOF 日志是以文件的形式存在的，当程序对 AOF 日志文件进行写操作时，实际上是将 内容写到了内核为文件描述符分配的一个内存缓存中，然后内核会异步将脏数据刷回到磁盘的    
8.  Linux 的 glibc 提供了 fsync(int fd)函数可以将指定文件的内容强制从内核缓存刷到磁 盘。只要 Redis 进程实时调用 fsync 函数就可以保证 aof 日志不丢失。    
9. 所以在生产环境的服务器中， Redis 通常是每隔 1s 左右执行一次 fsync 操作，周期1s 是可以配置的。这是在数据安全性和性能之间做了一个折中，在保持高性能的同时，尽可能 使得数据少丢失 。
10. Redis 4.0带来了一个新的持久化选项——混合持久化。将 rdb 文 件的内容和增量的 AOF 日志文件存在一起。这里的 AOF 日志不再是全量的日志，而是自 持久化开始到持久化结束的这段时间发生的增量 AOF 日志，通常这部分 AOF 日志很小。

### 原理 4：雷厉风行 —— 管道        

这个技术本质上是由客户端提供的。其实很简单，就是连续调用write发指令，到时可以一起读结果。不用说每次write-read-write-read这样子。

### 原理 5：同舟共济 —— 事务    

1. 所有的指令在 exec 之前不执行，而是缓存在 服务器的一个事务队列中，服务器一旦收到 exec 指令，才开执行整个事务队列，执行完毕 后一次性返回所有指令的运行结果。因为 Redis 的单线程特性，它不用担心自己在执行队列 的时候被其它指令打搅，可以保证他们能得到的「原子性」执行。
2. Redis事务在遇到指令执行失败后，后面的指令还继续执行。Redis 的事务根本不能算「原子性」，而仅仅是满足了事务的「隔 离性」，隔离性中的串行化——当前执行的事务有着不被其它事务打断的权利。
3. Redis事务在发送每个指令时都需要一次网络读写，我们可以通过使用管道让他变成单次IO操作。
4. 分布式锁是一种悲观锁 。我们可以通过watch来实现乐观锁。
5. watch 会在事务开始之前盯住 1 个或多个关键变量，当事务执行时，也就是服务器收到 了 exec 指令要顺序执行缓存的事务队列时， Redis 会检查关键变量自 watch 之后，是否被 修改了 (包括当前事务所在的客户端)。如果关键变量被人动过了， exec 指令就会返回 null    

### 原理 6：小道消息 —— PubSub    

1. 挂掉的消费者重新连上的时候，这断连期间生产者发送的消息，对于这个消费者来说就是彻底丢失了。    
2. 如果 Redis 停机重启， PubSub 的消息是不会持久化的    
3. 正是因为 PubSub 有这些缺点，它几乎找不到合适的应用场景    

### 原理 7：开源节流 —— 小对象压缩    

Redis 如果使用 32bit 进行编译，内部所有数据结构所使用的指针空间占用会少一半， 如果你对 Redis 使用内存不超过 4G，可以考虑使用 32bit 进行编译，可以节约大量内存。

### 原理 8：有备无患 —— 主从同步    

1. 分布式系统的节点往往都是分布在不同的机器上进行网络隔离开的，这意味着必然会有 网络断开的风险，这个网络断开的场景的专业词汇叫着「网络分区」。
2. 有了主从，当 master 挂 掉的时候，运维让从库过来接管，服务就可以继续，否则 master 需要经过数据恢复和重启 的过程，这就可能会拖很长的时间，影响线上业务的持续服务。    
3. Redis 的主从数据是异步同步的，所以分布式的 Redis 系统并不满足「一致性」要求。Redis 保证「最终一致性」。
4. 快照同步和增量同步。
5. redis从库只能读不可以修改
6. 如果你将 Redis 只用来做缓存，跟 memcache 一样来对 待，也就无需要从库做备份，挂掉了重新启动一下就行。但是只要你使用了 Redis 的持久化 功能，就必须认真对待主从复制，它是系统数据安全的基础保障。   

### 集群 1：李代桃僵 —— Sentinel（哨兵）     

客户端通过哨兵来获取主库的IP，哨兵监控集群状态和选主。

### 集群 2：分而治之 —— Codis    

在大数据高并发场景下，单个 Redis 实例往往会显得捉襟见肘。单个Redis实例内存空间不可以过大，单个 Redis 实例只能利用单个核 心。

Codis 是 Redis 集群方案之一。

Codis 上挂接的所有 Redis 实例构成一个 Redis 集群，当集群空间不足时，可以通过动 态增加 Redis 实例来实现扩容需求。    

### 集群 3：众志成城 —— Cluster    

RedisCluster 是 Redis 的亲儿子，它是 Redis 作者自己提供的 Redis 集群化方案。    

网络抖动就是非常常见的一种现象，突然之间部分连接变得不可访问，然后很快又恢复正 常。    

### 拓展 1：耳听八方 —— Stream    

1. Stream，它是一个新的强大的支持多播的可持久化的消息队列 
2. Stream 的消费模型借鉴了 Kafka 的消费分组的概念，它弥补了 Redis Pub/Sub 不能持 久化消息的缺陷。但是它又不同于 kafka， Kafka 的消息可以分 partition，而 Stream 不行。 如果非要分 parition 的话，得在客户端做，提供不同的 Stream 名称，对消息进行 hash 取 模来选择往哪个 Stream 里塞。       

### 拓展 2：无所不知 —— Info 指令    

### 拓展 3：拾遗漏补 —— 再谈分布式锁    

### 拓展 4：朝生暮死 —— 过期策略    

1. redis 会将每个设置了过期时间的 key 放入到一个独立的字典中，以后会定时遍历这个 字典来删除到期的 key。除了定时遍历之外，它还会使用惰性策略来删除过期的 key，所谓 惰性策略就是在客户端访问这个 key 的时候， redis 对 key 的过期时间进行检查，如果过期了就立即删除。定时删除是集中处理，惰性删除是零散处理。
2. 采用了一种简单的贪心策略来定时扫描删除    
3. 业务开发人员一定要注意过期时间，如果有大批量的 key 过期，要给过期时间设置 一个随机范围，而不能全部在同一时间过期。
4. 从库不会进行过期扫描，从库对过期的处理是被动的。主库在 key 到期时，会在 AOF 文件里增加一条 del 指令，同步到所有的从库，从库通过执行这条 del 指令来删除过期的 key。可能出现主从数据的不一致 。

### 拓展 5：优胜劣汰 —— LRU    

### 拓展 6：平波缓进 —— 懒惰删除    

1. Redis 内部实际上并不是只有一个主线程，它还有几个异步线程专门用来 处理一些耗时的操作。  

### 拓展 8：居安思危 —— 保护 Redis      

1. 耗时命令重命名或禁止
2. 不可暴露公网

### 拓展 9：隔墙有耳 —— Redis 安全通信    

A机房的进程读取B机房的Redis数据库，跨机房传输，传输数据不安全。

### 源码 1：极度深寒 —— 探索「字符串」内部结构    

```
struct SDS<T> {
T capacity; // 数组容量
T len; // 数组长度
byte flags; // 特殊标识位，不理睬它
byte[] content; // 数组内容
}
```

len 和 capacity 可以使用 byte 和 short 来表示， Redis 为了对内存做极致的优化，不同 长度的字符串使用不同的结构体来表示。    

```
//redis对象头结构体
struct RedisObject {
int4 type; // 4bits
int4 encoding; // 4bits
int24 lru; // 24bits
int32 refcount; // 4bytes
void *ptr; // 8bytes， 64-bit system
} robj;
```

Redis 的字符串有两种存储方式，在长度特别短时，使用 emb 形式存储 (embeded)，当 长度超过 44 时，使用 raw 形式存储    

embstr 存储形式是这样一种存储形式，它将 RedisObject 对象头和 SDS 对 象连续存在一起，使用 malloc 方法一次分配。而 raw 存储形式不一样，它需要两次 malloc 

### 源码 2：极度深寒 —— 探索「字典」内部

```
//1个字典包含两个哈希表
struct dict {
...
dictht ht[2];
}
struct dictEntry {
void* key;
void* val;
dictEntry* next; // 链接下一个 entry
}
struct dictht {
dictEntry** table; // 二维
long size; // 第一维数组的长度
long used; // hash 表中的元素个数
...
}
```

### 源码 3：极度深寒 —— 探索「压缩列表」内部    

