#### 字典

##### 字典的实现方式

1. 最简单的就是使用链表或数组，但是这种方式只适用于元素个数不多的情况下；
2. 要兼顾高效和简单性，可以使用哈希表；
3. 如果追求更为稳定的性能特征，并且希望高效地实现排序操作的话，则可以使用更为复
   杂的平衡树； 

##### 字典定义

```c++
typedef struct dict {
// 特定于类型的处理函数
dictType *type;
// 类型处理函数的私有数据
void *privdata;
// 哈希表（2 个）
dictht ht[2];
// 记录 rehash 进度的标志，值为-1 表示 rehash 未进行
int rehashidx;
// 当前正在运作的安全迭代器数量
int iterators;
} dict;
```

```c++
typedef struct dictht {
// 哈希表节点指针数组（俗称桶， bucket）
dictEntry **table;
// 指针数组的大小
unsigned long size;
// 指针数组的长度掩码，用于计算索引值
unsigned long sizemask;
// 哈希表现有的节点数量
unsigned long used;
} dictht;
```

##### rehash

一次性rehash会延迟相应时长，Redis采用的是渐进式rehash。被动渐进式rehash则是每次执行一次添加、查找、删除操作，ht[0]->table 哈希表第一个不为空的索引上的所有节点就会全部迁移到 ht[1]->table 。

 #### 跳跃表

1. 跳跃表在 Redis 的唯一作用，就是实现有序集数据类型。 跳跃表将指向有序集的 score 值和 member 域的指针作为元素，并以 score 值为索引，对有序集元素进行排序。 
2. 跳跃表要和另一个实现有序集的结构（字典）分享 member 和 score 值，所以跳跃表只保存指向 member 和 score 的指针。 

#### 压缩列表

1. ziplist 节约内存的性质，它被哈希键、列表键和有序集合键作为初始化的底层实现来使用
2. ziplist 由连续的内存块构成

#### 有序集

##### 压缩列表和跳跃表的选择

如果第一个元素符合以下条件的话，就创建一个 REDIS_ENCODING_ZIPLIST 编码的有序集：

1. 服务器属性 server.zset_max_ziplist_entries 的值大于 0 （默认为 128 ）
2. 元素的 member 长度小于服务器属性 server.zset_max_ziplist_value 的值（默认为 64） 

##### 压缩列表使用

1. 每个有序集元素以两个相邻的 ziplist 节点表示，第一个节点保存元素的 member 域，第二个元素保存元素的 score 域。
2. 多个元素之间按 score 值从小到大排序，如果两个元素的 score 相同，那么按字典序对 member进行对比，决定那个元素排在前面，那个元素排在后面。 

##### 跳跃表使用

```c++
typedef struct zset {
// 字典
dict *dict;
// 跳跃表
zskiplist *zsl;
} zset;
```

1. zset 同时使用字典和跳跃表两个数据结构来保存有序集元素。 
2. 通过使用字典结构，并将 member 作为键， score 作为值，有序集可以在 O(1) 复杂度内：
   • 检查给定 member 是否存在于有序集（被很多底层函数使用）；
   • 取出 member 对应的 score 值（实现 ZSCORE 命令）。
3. 另一方面，通过使用跳跃表，可以让有序集支持以下两种操作：
   • 在 O(log N) 期望时间、 O(N) 最坏时间内根据 score 对 member 进行定位（被很多底层函数使用）；
   • 范围性查找和处理操作，这是（高效地）实现 ZRANGE 、 ZRANK 和 ZINTERSTORE等命令的关键。
4. 通过同时使用字典和跳跃表，有序集可以高效地实现按成员查找和按顺序查找两种操作。 

#### 哈希表

就是用了压缩列表，或者字典。

